import midas.event
import midas.frontend
import vxi11
import os.path
import math
import time
import struct
import datetime
import collections

"""
This frontend controls a Rohde & Schwarz RTM3000 series oscilloscope through midas.
See the README file for usage and explanation of the ODB parameters.

Most configuration is expected to be done manually on the scope, with this frontend
just providing tools to read out waveforms and/or measurement results during a run.

If you do not have the vxi11 python module, you can install it with 
`pip install python-vxi11`.
"""

num_chans = 4
num_meas = 8

class RTM3000:
    """
    Simple wrapper that connects to the scope via TCP:IP, and provides
    tools to parse responses from the scope.
    """
    def __init__(self, ip):
        """
        Members:
            
        * inst (`vxi11.Instrument`) - connection to the scope
        """
        self.inst = vxi11.Instrument(ip)
        print("Connected to %s" % self.get_str("*IDN?"))
        
    def run(self, cmd):
        """
        Send a command to the scope.
        """
        self.inst.write(cmd)
    
    def get_binary(self, query):
        """
        Send a command to the scope and read the response as binary data.
        """
        if query.find("?") == -1:
            raise ValueError("Queries should include a ?")
        
        self.run(query)
        return self.inst.read_raw()
    
    def get_str(self, query):
        """
        Send a command to the scope and read the response as a string.
        """
        if query.find("?") == -1:
            raise ValueError("Queries should include a ?")
        
        return self.inst.ask(query)
        
    def get_int(self, query):
        """
        Send a command to the scope and parse the response as an integer.
        """
        return int(self.get_str(query))
        
    def get_float(self, query):
        """
        Send a command to the scope and parse the response as a float.
        """
        return float(self.get_str(query))
        
    def extract_binary_size_info(self, data):
        """
        Send a command to the scope and parse the response as a float.
        """
        if chr(data[0]) != '#':
            raise ValueError("Unexpected format of binary data: %s" % data[:10])

        num_chars = int(chr(data[1]))
        payload_size = int("".join(chr(data[x]) for x in range(1, 1+num_chars)))
        payload_start = 2 + num_chars
        return (payload_size, payload_start)
        
    def get_waveform(self, chan):
        """
        Get the waveform for a channel (1 to 4).
        
        Returns:
            3-element tuple of:
                * The waveform data (list of floats)
                * Timestep between each point (in seconds)
                * Time of the first point relative to the trigger (in seconds)
        """
        if chan < 1 or chan > num_chans:
            raise ValueError("Channel numbers should be in range 1 to %d" % num_chans)
        
        self.run("FORM REAL")
        self.run("FORM:BORD LSBF")
        self.run("CHAN%d:DATA:POIN DEF")
        data = self.get_binary("CHAN%d:DATA?" % chan)
        x_inc = self.get_float("CHAN:DATA:XINC?")
        x0 = self.get_float("CHAN:DATA:XOR?")
        
        (payload_size, payload_start) = self.extract_binary_size_info(data)
        payload = data[payload_start:-1]
        num_floats = len(payload) / 4
        
        wf = struct.unpack("%df" % num_floats, payload)
        return (list(wf), x_inc, x0)
    
    def enable_measurement_stats(self, enable):
        """
        Enable/disable computation of statistics for any enabled measurements.
        """
        self.run("MEAS:STAT %s" % ("ON" if enable else "OFF"))
    
    def get_measurement(self, place, with_stats=True):
        """
        Get the current measurement in place 1-8.
        
        If `with_stats` is False, returns:
            A single number, the most recent value
            
        If `with_stats` is True, returns:
            A 6-element tuple of:
                * The most recent value
                * The minimum value seen this run
                * The maximum value seen this run
                * The average value seen this run
                * The std. deviation of values seen this run
                * The number of measurements made this run
        """
        if place < 1 or place > num_meas:
            raise ValueError("Measurements places should be in range 1 to %d" % num_meas)
        
        if self.get_int("MEAS%d?" % place) == 0:
            # Measurement not active. Avoid a lengthy timeout by returning early
            return None
        
        # Measurement active - get the value
        latest = self.get_float("MEAS%d:RES?" % place)
        
        if math.isnan(latest):
            return None
        
        if with_stats:
            mini = self.get_float("MEAS%d:RES:NPE?" % place)
            maxi = self.get_float("MEAS%d:RES:PPE?" % place)
            mean = self.get_float("MEAS%d:RES:AVG?" % place)
            stddev = self.get_float("MEAS%d:RES:STDD?" % place)
            count = self.get_float("MEAS%d:RES:WFMC?" % place)
            return (latest, mini, maxi, mean, stddev, count)
        else:
            return latest
    
    def reset_measurement_stats(self):
        """
        Reset all the measured statistics on the scope.
        """
        self.run("MEAS:STAT:RES")
    
    def save_screen_capture(self, screenshot_path="/tmp/screenshot.png"):
        """
        Save a screen capture of the current scope display to the specified location.
        Allowed extensions are .png, .bmp and .gif.
        
        Be warned that it takes ~1s for the screenshot to be acquired!
        """
        (base, ext) = os.path.splitext(screenshot_path)
        
        if ext.startswith("."):
            ext = ext[1:]
        
        if ext.upper() not in ["PNG", "BMP", "GIF"]:
            raise ValueError("Invalid screenshot extension '%s'. Only png/bmp/gif supported by the scope." % ext)
        
        self.run("HCOP:LANG %s" % ext.upper())
        data = self.get_binary("HCOP:DATA?")
        
        (payload_size, payload_start) = self.extract_binary_size_info(data)
        payload = data[payload_start:-1]
        
        with open(screenshot_path, "wb") as f:
            f.write(payload)
        
    def start_single(self, count=1):
        """
        Run until we've seen the specified number of triggers.
        """
        self.run("ACQ:NSIN:COUN %d" % count)
        self.run("*ESE 1; SING")
    
    def start_cont(self):
        """
        Start the scope in continuous triggering mode.
        """
        self.run("RUN")
        
    def stop(self):
        """
        Stop the scope acquisition.
        """
        self.run("STOP")
    
    def has_seen_trigger(self):
        """
        Return whether the scope has seen a trigger since you last asked.
        """
        return self.get_int("*OPC; *ESR?") == 1
        
        # Bit 3 of STAT:OPER means "waiting for trigger"
        #return not (self.get_int("STAT:OPER:COND?") & 8)
    
    def force_trigger_now(self):
        """
        Force the scope to trigger.
        """
        self.run("*TRG")
        
    def set_horizontal_params(self, len_us, max_num_samples, offset_us):
        """
        Set parameters that affect the sampling rate, trace length and trace position.
        
        It is NOT possible to directly set the sampling rate or number of samples.
        However, it is possible to set the time range, and an upper limit on the number 
        of samples to read out (the record length). The scope will determine an appropriate 
        sampling rate based on the visible time range and the record length.
        
        The actual number of samples read out is then "sampling rate chosen by the scope"
        multiplied by "time range chosen by the user".
        
        Args:
            
        * len_us (float) - Visible trace length in us
        * max_num_samples (int) - Maximum number of samples to read out (sensible range
            that matches what the scope wants to do is 5000 - 100000)
        * offset_us (float) - Position of the trigger relative to the middle of the screen.
            Positive values shift the trigger to the left.
            
        Returns:
            2-element tuple of:
                * The sampling rate chosen by the scope
                * The number of samples that will be read out
        """
        # Displayed width
        self.run("TIM:RANG %f" % (len_us * 1e-6))
        
        # Reference point (middle of display)
        self.run("TIM:REF 50")
        
        # Position offset in secs; positive values shift trigger time left
        self.run("TIM:POS %f" % (offset_us * 1e-6))
                    
        # Find record length that's big enough for the number of
        # samples user wants to read out
        allowed_record_lengths = [5e3, 10e3, 20e3, 50e3, 100e3, 200e3, 500e3, 1e6, 2e6, 5e6, 10e6, 20e6, 40e6, 80e6]
        record_length = max_num_samples
        record_idx = None
            
        for i, r in enumerate(allowed_record_lengths):
            if r >= max_num_samples:
                record_length = r
                record_idx = i
                break
                
        if record_idx is None:
            record_length = allowed_record_lengths[-1]
        
        # Set the number of samples in the record
        self.run("ACQ:POIN:VAL %d" % record_length)
        
        # See what sample rate the scope has chosen based on our trace 
        sample_rate = self.get_float("ACQ:SRAT?")
        readout_length = int(sample_rate * len_us * 1e-6)
        
        if readout_length > max_num_samples and record_idx > 0:
            # We ended up with more samples than the user wanted.
            # Go down to a lower record length and try again
            record_length = allowed_record_lengths[record_idx - 1]
            self.run("ACQ:POIN:VAL %d" % record_length)
            sample_rate = self.get_float("ACQ:SRAT?")
            readout_length = int(sample_rate * len_us * 1e-6)
            
        return (sample_rate, readout_length)
        
    def set_vertical_params(self, chan, range_V, offset_V):
        """
        Set the vertical range and offset for a channel. Positive offset values
        shift the trace down.
        """
        self.run("CHAN%d:RANG %f" % (chan, range_V))
        
        # Positive values shift trace down
        self.run("CHAN%d:OFFS %f" % (chan, offset_V))
    
        
class RTM3000Equipment(midas.frontend.EquipmentBase):
    def __init__(self, client):
        equip_name = "RTM3000"

        default_common = midas.frontend.InitialEquipmentCommon()
        default_common.equip_type = midas.EQ_POLLED
        default_common.buffer_name = "SYSTEM"
        default_common.trigger_mask = 0
        default_common.event_id = 3000
        default_common.period_ms = 100
        default_common.read_when = midas.RO_RUNNING
        default_common.log_history = 0

        try:
            screenshot_path = os.path.join(client.obd_get("/Logger/Data dir"), "rtm3000.png")
        except:
            screenshot_path = "/tmp/rtm3000.png"

        self.default_settings = collections.OrderedDict([
                                    ("IP address", "142.90.118.150"),
                                    ("Readout channels", [True, False, False, False]),
                                    ("Readout measurements", [False] * num_meas),
                                    ("Prescale factor", 1),
                                    ("Force trigger now", False),
                                    ("Stop scope at end-of-run", False),
                                    ("Trace length (us)", 12.0),
                                    ("Max num samples", 10000),
                                    ("Horizontal position (us)", 0),
                                    ("Vertical range (V)", [1., 1., 1., 1.]),
                                    ("Vertical offset (V)", [0., 0., 0., 0.]),
                                    ("Screenshot location", screenshot_path)])

        midas.frontend.EquipmentBase.__init__(self, client, equip_name, default_common, self.default_settings)
        
        self.scope = RTM3000(self.settings["IP address"])
        
        # ODB location for storing the sampling rate and # of samples to read out, as
        # calculated by the scope.
        self.odb_computed_dir = self.odb_settings_dir.replace("Settings", "Computed")
        
        default_computed = {  "Sampling rate (Hz)": 0.0, 
                              "Readout length (samples)": 0
                            }
        
        self.client.odb_set(self.odb_computed_dir, default_computed, update_structure_only=True)
        
        self.wf_len_samples = -1
        self.prescale_counter = 0
        
        self.update_all()
        
    def update_screen_capture(self):
        """
        Take a screenshot of the scope, saving it to the location specified in the ODB.
        """
        self.scope.save_screen_capture(self.settings["Screenshot location"])
        
    def update_horizontal(self):
        """
        Update the time-based parameters, based on settings in the ODB.
        Also update the sampling rate and readout length the scope calculated based
        on these settings.
        """
        (rate, num_samps) = self.scope.set_horizontal_params(self.settings["Trace length (us)"], self.settings["Max num samples"], self.settings["Horizontal position (us)"])
        self.wf_len_samples = num_samps
        
        self.client.odb_set(self.odb_computed_dir + "/Sampling rate (Hz)", rate)
        self.client.odb_set(self.odb_computed_dir + "/Readout length (samples)", self.wf_len_samples)

    def update_vertical(self, chan):
        """
        Update the vertical parameters for a channel, based on the ODB settings.
        """
        self.scope.set_vertical_params(chan, self.settings["Vertical range (V)"][chan - 1], self.settings["Vertical offset (V)"][chan - 1])

    def update_measurement_enabled(self):
        """
        If the user wants to read out any measurements, ensure that stats 
        calculation is enabled.
        """
        any_enabled = any(self.settings["Readout measurements"])
        self.scope.enable_measurement_stats(any_enabled)

    def update_all(self):
        """
        Update all scope parameters based on the current ODB settings.
        """
        self.update_horizontal()

        for i in range(num_chans):
            self.update_vertical(i + 1)

        self.update_measurement_enabled()
            
    def detailed_settings_changed_func(self, path, idx, new_val):
        """
        Hotlink called whenever an ODB setting changes.
        """
        key = path.split("/")[-1]
        
        if key == "IP address":
            self.scope = RTM3000(self.settings["IP address"])
            self.update_all()
            
        if key == "Force trigger now" and new_val == True:
            self.scope.force_trigger_now()
            self.client.odb_set(path, False)
            
        if key in ["Trace length (us)", "Max num samples", "Horizontal position (us)"]:
            self.update_horizontal()
            
        if key in ["Vertical range (V)", "Vertical offset (V)"]:
            self.update_vertical(idx + 1)
            
        if key == "Readout measurements":
            self.update_measurement_enabled()
            
    def arm_scope(self):
        self.scope.start_single()
            
    def begin_of_run(self):
        """
        BOR transition. Update all scope parameters, then start it.
        """
        self.prescale_counter = 0
        self.scope.stop()
        
        if self.settings["Prescale factor"] <= 0:
            raise ValueError("Invalid pre-scale factor %d. Must be >= 1" % self.settings["Prescale factor"])
        
        self.update_all()
        self.scope.reset_measurement_stats()
        self.arm_scope()
            
    def end_of_run(self):
        """
        EOR transition. Stop the scope if the user asked for that.
        """
        if self.settings["Stop scope at end-of-run"]:
            self.scope.stop()
        else:
            self.scope.start_cont()
            
    def poll_func(self):
        """
        Return True if it's time to read out data from the scope.
        """
        seen = self.scope.has_seen_trigger()
        retval = False
        
        if seen:
            # Pre-scale logic to read first trigger, then every N triggers after that
            if self.prescale_counter % self.settings["Prescale factor"] == 0:
                self.prescale_counter = 0
                retval = True
            else:
                # Re-arm scope to get next trigger.
                self.arm_scope()
                
            self.prescale_counter += 1
            
        return retval
    
    def readout_func(self):
        """
        Read out data (waveforms and/or measurement stats) from the scope.
        """
        event = midas.event.Event()
        
        # Read out waveforms
        for i, do_readout in enumerate(self.settings["Readout channels"]):
            if not do_readout:
                continue
            
            chan = i + 1
            
            try:
                (wf, x_inc, x0) = self.scope.get_waveform(chan)
            except vxi11.vxi11.Vxi11Exception:
                # Timeout reading the data. Scope reset? Ignore event.
                print("Timeout waiting for data from channel %d" % chan)
                self.restart_scope()
                return None
                
            # The actual waveform sometimes gives a few more samples than we expect (0-8 more).
            # Not sure why this is (no obvious modulo logic). Trim as needed.
            event.create_bank("WFC%01d" % chan, midas.TID_FLOAT, wf[:self.wf_len_samples])
            
            # Add a "header" bank with time offset of first sample, and increment between samples
            event.create_bank("HDR%01d" % chan, midas.TID_DOUBLE, [x_inc, x0])
            
        # Read out measurements
        for i, do_readout in enumerate(self.settings["Readout measurements"]):
            if not do_readout:
                continue
            
            place = i + 1
            meas = self.scope.get_measurement(place)
            
            if meas is not None:
                (latest, mini, maxi, mean, stddev, count) = meas
                event.create_bank("MSR%01d" % chan, midas.TID_DOUBLE, [latest, mini, maxi, mean, stddev, count])
        
        self.arm_scope()
        
        return event
    
class RTM3000Frontend(midas.frontend.FrontendBase):
    """
    Frontend with a single equipment. 
    
    Registers a JRPC handler in case someone wants to write an interactive custom page.
    """
    def __init__(self):
        midas.frontend.FrontendBase.__init__(self, "fe_rtm3000")

        self.add_equipment(RTM3000Equipment(self.client))
        self.client.register_jrpc_callback(self.rpc_handler, True)

    def begin_of_run(self, run_number):
        self.equipment["RTM3000"].begin_of_run()

    def end_of_run(self, run_number):
        self.equipment["RTM3000"].end_of_run()

    def rpc_handler(self, client, cmd, args, max_len):
        """
        JRPC handler we register with midas. This will generally be called when
        a user clicks a button on the associated webpage.

        Args:

        * client (`midas.client.MidasClient`)
        * cmd (str) - The command the user wants us to do.
        * args (str) - Stringified JSON of any arguments for this command.
        * max_len (int) - Maximum length of response the user will accept.

        Returns:
            2-tuple of (int, str) for (status code, message)

        Accepted commands:
        * start_single/start_cont/stop/trigger - Simple preset commands to run on scope.
        * run_command - Custom command to run on scope (args should JSON-encode command in "query" param).
        * update_screen_capture - Request that a new screencap is taken now.
        """
        ret_int = midas.status_codes["SUCCESS"]
        ret_str = "OK"

        if cmd == "start_single":
            self.equipment["RTM3000"].scope.start_single()
        if cmd == "start_cont":
            self.equipment["RTM3000"].scope.start_cont()
        if cmd == "stop":
            self.equipment["RTM3000"].scope.stop()
        elif cmd == "trigger":
            self.equipment["RTM3000"].scope.force_trigger_now()
        elif cmd == "run_command":
            query = json.loads(args)["query"]

            if query.find("?") == -1:
                self.equipment["RTM3000"].scope.run(query)
            else:
                ret_str = self.equipment["RTM3000"].scope.get_str(query)
        elif cmd == "update_screen_capture":
            self.equipment["RTM3000"].update_screen_capture()
        else:
            raise ValueError("Unexpected command %s" % cmd)

        return (ret_int, ret_str)

if __name__ == "__main__":
    fe = RTM3000Frontend()
    fe.run()

