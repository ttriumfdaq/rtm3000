# Midas frontend for the Rohde and Schwarz RTM3000-series oscilloscope

This frontend is a minimal version for integrating the scope with midas for a desktop experiment. Only the parameters that directly affect readout of the scope are implemented in the ODB; for everything else you should use the scope itself (either physically, or via the web interface it provides).

## Requirements

The code uses one external package (vxi11), which can be installed from pip with `pip install python-vxi11`. 

It also requires the [midas python wrapper](https://midas.triumf.ca/MidasWiki/index.php/Python), which I assume is already in your PYTHONPATH or installed with pip.

The code will work with python 3.6+ only.

## Background on continuous mode operation

I have found NO good way to reliably determine whether the scope has seen a new trigger since we last checked. Bit 3 of STAT:OPER means "waiting for trigger", but seems to always be set if you have a low trigger rate - even polling at a high rate the scope always claimed to be waiting.

I can not find a command that tells you the time of the last trigger, the number of triggers seen this run etc. If we just blindly read data in continuous mode we could end up reading the same data multiple times, or experiencing long timeouts waiting for non-existant data.

However, in "single acquisition" mode, we can use the ESE/OPC/ESR commands to see when the next trigger has been acquired. This means that we stop and start the scope each time we get a trigger, which introduces a fairly significant deadtime. The maxmimum repetition rate appears to be a few Hz.

## ODB parameter explanations

* `IP address` - IP address of the scope
* `Readout channels` - Which channels (1-4) to read out (array index is offset by 1; 0-3)
* `Readout measurements` - Which measurements (1-8) to read out (array index is offset by 1; 0-3). Set up the measurements on the scope.
* `Prescale factor` - Only read data for 1 in N triggers. Particularly useful if you have a high trigger rate and want to acquire a lot of stats for the summary measurements, but still take some waveforms (you will get waveform data for 1 in N triggers, but the mean/min/max etc values of the measurements will include data from all triggers).
* `Force trigger now` - Hotlink to force a trigger to be taken
* `Stop scope at end-of-run` - Whether to stop the scope or put it in continuous trigger mode when the run stops
* `Trace length (us)` - Length of waveform for all channels
* `Max num samples` - Maximum number of samples to read out when digitizing the waveform. The scope will choose an appropriate sampling rate and the exact number of samples for you (based on this setting and the trace length). There is no way to set the sampling rate directly. You can view the chosen sampling rate and number of samples in the ODB at `/Equipment/RTM3000/Computed`.
* `Horizontal position (us)` - Horizontal offset of the trigger time, relative to the center of the display. Positive values move the trigger to the left.
* `Vertical range (V)` - Vertical range of each channel.
* `Vertical offset (V)` - Vertical offset of each channel. Positive values move the trace down.
* `Screenshot location` - If you use the JRPC command `update_screen_capture`, where to save the screenshot to.

## Data format

Waveform data is written in the `WFC1` and `HDR1` banks (for channel 1). 

* `WFC1` is simply a list of floats for the waveform, all in volts. 
* `HDR1` is two doubles, both in seconds: 1) the time difference between each point. 2) the offset of the first point relative to the trigger.

Measurement data is written in the `MSR1` bank (for measurement 1).

* `MSR1` has 6 doubles: 1) latest value. 2) minimum value. 3) maximum value. 4) mean. 5) stddev. 6) number of triggers analyzed.